﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_ProceduralGenerator : MonoBehaviour
{
    public GameObject[] squares;
    private GameObject actualsquare;


    public int seed;
    public int hole;
    public int copper;
    public int diamond;


    public float amp;
    public float freq;

    void Start()
    {
        generate();

    }

    private void generate()
    {
        Vector3 pos = this.transform.position;

        int fil = 100;
        int col = 200;

        //ens movem pel mapa com una matriu
        for (int x = 0; x < col; x++)
        {
            for (int y = 0; y < fil; y++)
            {


                float probhole = Mathf.PerlinNoise((pos.x + x + hole) / freq, (pos.y + y + hole) / freq);
                float probcopper = Mathf.PerlinNoise((pos.x + x + copper) / freq, (pos.y + y + copper) / freq);
                float probdiamond = Mathf.PerlinNoise((pos.x + x + diamond) / freq, (pos.y + y + diamond) / freq);

                float z = Mathf.PerlinNoise((pos.x + x + seed) / freq, (pos.y + y + seed) / freq);
                z = z * amp;
                z -= 2;

                if (z > 40 * (amp / 100) && y > 65)
                {
                    actualsquare = squares[1];
                }
                else
                {
                    if (probcopper > 0.70f)
                        actualsquare = squares[4];
                    else if (probdiamond > 0.80f && y < 20)
                        actualsquare = squares[5];
                    else
                        actualsquare = squares[2];

                    if (probhole > 0.55f)
                    {
                        actualsquare = squares[3];
                    }
                }


                GameObject newBlock = Instantiate(actualsquare);
                newBlock.transform.position = new Vector3(pos.x + x, pos.y + y, 0);

                //for (float i = y; i < amp * 30 / 100; i++)
                //{
                //    GameObject aguaPorFavor = Instantiate(aigua);
                //    aguaPorFavor.transform.position = new Vector3(pos.x + x, i, pos.z + z);
                //}
            }
        }
    }

}
